# Vue.js custom plugin sample

## Install

```
npm install
```

## Build

```
npm run build
```

## Run

Open `index.html` in the browser.
